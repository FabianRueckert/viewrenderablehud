package com.google.ar.sceneform.samples.hellosceneform

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Point
import android.os.Build
import android.support.annotation.LayoutRes
import android.support.annotation.RawRes
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import com.google.ar.sceneform.Node
import com.google.ar.sceneform.SceneView
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.rendering.ViewRenderable
import kotlinx.android.synthetic.main.hud.view.*
import java.util.concurrent.CompletableFuture

@TargetApi(Build.VERSION_CODES.N)
internal class ViewRenderableHud(
        private val context: Context,
        sceneView: SceneView
) {
    private val node = Node()
    private var captureExperienceRenderable: ViewRenderable? = null

    init {
        node.collisionShape = null
        node.setParent(sceneView.scene.camera)

        // NOTE: Though HUD is rendered in device space, frustum culling
        // is still applied, thus move right in front of near clipping plane.
        // worldPosition is used because nearClipPlane returns world units
        node.worldPosition = Vector3.add(
                sceneView.scene.camera.worldPosition,
                sceneView.scene.camera.forward.scaled(sceneView.scene.camera.nearClipPlane + .001f)
        )

        loadViewRenderable(R.layout.capture_experience_hud) { renderable ->
            captureExperienceRenderable = renderable
            node.renderable = renderable
            renderable.view?.title?.text = "View Renderable"
        }
    }

    internal fun buildViewRenderable(
            @LayoutRes layout: Int,
            fullscreen: Boolean = true,
            @RawRes renderable: Int = com.google.ar.sceneform.rendering.R.raw.sceneform_view_renderable,
            horizontalAlignment: ViewRenderable.HorizontalAlignment = ViewRenderable.HorizontalAlignment.CENTER,
            verticalAlignment: ViewRenderable.VerticalAlignment = ViewRenderable.VerticalAlignment.CENTER
    ): CompletableFuture<ViewRenderable> {
        val windowManager =
                context.applicationContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val parent = FrameLayout(context) as ViewGroup
        if (fullscreen) {
            val size = Point()
            windowManager.defaultDisplay.getRealSize(size)
            parent.layoutParams = FrameLayout.LayoutParams(size.x, size.y)
        }
        LayoutInflater.from(context).inflate(layout, parent)

        return ViewRenderable.builder()
                .setSource(context, renderable)
                .setView(context, parent)
                .setHorizontalAlignment(horizontalAlignment)
                .setVerticalAlignment(verticalAlignment)
                .build()
    }

    private fun loadViewRenderable(@LayoutRes layout: Int, success: (ViewRenderable) -> Unit) {
        buildViewRenderable(layout, renderable = R.raw.hud)
                .exceptionally { e ->
                    e.printStackTrace()
                    null
                }
                .thenAccept {
                    it.isShadowCaster = false
                    it.isShadowReceiver = false
                    it.collisionShape = null

                    success(it)
                }
    }
}
